/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.lab3;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab3 {
    
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static char currentPlayer = 'X';
    static int row,col;
    
    static void printWelcomeOX(){
        System.out.println("Welcome to OX");
    }
    
    static void printTable(){
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println();
        }
    }
    
    static void printTurn(){
        System.out.println("Turn "+currentPlayer);
    }
    
    static void inputRowColum(){
        while(true){
            Scanner kb = new Scanner(System.in);
            System.out.print("Please input row col : ");
            row = kb.nextInt();
            col = kb.nextInt();
            if(table[row-1][col-1]=='-'){
                table[row-1][col-1] = currentPlayer;
                break;
            }
        }
    }
    
    static void switchPlayer(){
        if(currentPlayer=='X'){
            currentPlayer='O';
        }else{
            currentPlayer='X';
        }
    }
    
    static boolean checkWin(char[][] table,char currentPlayer) {    
        if(checkRow(table,currentPlayer)||checkCol(table,currentPlayer)||checkDiagonal(table,currentPlayer)){
            return true;
        }
        return false;
    }
    
    static boolean checkRow(char[][] table,char currentPlayer){
        for(int row=0; row<3; row++){
            if(checkRow(table,currentPlayer,row)){
                return true;
            }
        }
        return false;
    }
    
    static boolean checkRow(char[][] table,char currentPlayer,int row) {
        for(int col=0; col<3; col++) {
            if(table[row][col]!=currentPlayer)
                return false;
        }
        return true;
    }
    
    static boolean checkCol(char[][] table,char currentPlayer){
        for(int col=0; col<3; col++){
            if(checkCol(table,currentPlayer,col)){
                return true;
            }
        }
        return false;
    }
    
    static boolean checkCol(char[][] table,char currentPlayer,int col){
        for(int row=0; row<3; row++){
            if(table[row][col]!=currentPlayer){
                return false;
            }
        }
        return true;
    }
    
    static boolean checkDiagonal(char[][] table,char currentPlayer){
        if(table[0][0]==currentPlayer&&table[1][1]==currentPlayer&&table[2][2]==currentPlayer){
            return true;
        }
        if(table[0][2]==currentPlayer&&table[1][1]==currentPlayer&&table[2][0]==currentPlayer){
            return true;
        }
        return false;
    }
    
    static boolean checkDraw(char[][] table,char currentPlayer){
        if(checkDraw(table)){
            if(checkWin(table,currentPlayer)){
                return false;
            }
            return true;
        }
        if(checkWin(table,currentPlayer)){
            return false;
        }
        return false;
    }
    
    static boolean checkDraw(char[][] table){
        int round=0;
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                if(table[i][j]!='-'){
                    round++;
                }
            }
        }
        
        if(round==9){
            return true;
        }
        return false;
    }
    
    static void showResult(char[][] table,char currentPlayer){
        if(checkDraw(table,currentPlayer)){
            System.out.println("Draw!");
        }else{
            System.out.println(currentPlayer+" win!");
        }

    }
    
    static boolean inputContinue(){
        Scanner kb = new Scanner(System.in);
        System.out.print("continue(y/n) : ");
        char isCon = kb.next().charAt(0);
        if(isCon=='y'){
            return true;
        }
        return false;
    }
    
    static void reTable(){
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                table[i][j] = '-';
            }
        }
    }
    
    public static void main(String[] args){
        printWelcomeOX();
        while(true){
            while(true){
                printTable();
                printTurn();
                inputRowColum();
                if(checkWin(table,currentPlayer)){
                    printTable();
                    showResult(table,currentPlayer);
                    break;
                }
                if(checkDraw(table,currentPlayer)){
                    printTable();
                    showResult(table,currentPlayer);
                    break;
                }
                switchPlayer();
            }
            if(inputContinue()){
                reTable();
            }else{
                break;
            }
        }
    }
    

        
}